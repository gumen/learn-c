#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

// source: https://en.wikibooks.org/wiki/C_Programming/stdbool.h
/* compile with: gcc -std=c99 */

int main(void) {
	bool keep_going = true;	// Could be `bool keep_going = 1;`

	while(keep_going) {
		printf("Run as long as keep_going is true.\n");
		keep_going = false;	// Could be `keep_going = 0;`
	}

	printf("Stopping!\n");
	return EXIT_SUCCESS;
}
