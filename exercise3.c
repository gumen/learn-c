#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

int main(int argc, char *argv[]) {
	bool show_help = false;
	bool show_version = false;

	for(int i = 0; i < argc; i++) {
		printf("argv[%d] %s\n", i, argv[i]);

		if(strncmp(argv[i], "--help", 6) == 0 ||
		   strncmp(argv[i], "help",   4) == 0 ||
		   strncmp(argv[i], "-h",     2) == 0 ||
		   strncmp(argv[i], "-?",     2) == 0 ||
		   strncmp(argv[i], "?",      1) == 0)
			show_help = true;

		if(strncmp(argv[i], "--version", 9) == 0 ||
		   strncmp(argv[i], "version",   7) == 0 ||
		   strncmp(argv[i], "--ver",     5) == 0 ||
		   strncmp(argv[i], "ver",       3) == 0 ||
		   strncmp(argv[i], "-v",        2) == 0)
			show_version = true;
	}

	if (show_help) {
		printf("Show only help\n");
		return 0;
	}

	if (show_version) {
		printf("Show version\n");
	}

	return EXIT_SUCCESS;
}
