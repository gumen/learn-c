#include <stdio.h>
/*
http://www.c4learn.com/c-programming/c-void-pointers/
http://www.c4learn.com/c-programming/c-dereferencing-void-pointers/
http://www.c4learn.com/c-programming/c-size-of-void-pointers/
*/

int main(void) {
	void *p;

	char  a = 'a';
	int   b = 1;
	float c = 1.5;

	p = &a;
	printf("%c\n", *((char*)p));
	p = &b;
	printf("%d\n", *((int*)p));
	p = &c;
	printf("%f\n", *((float*)p));

	return 0;
}
