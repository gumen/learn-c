#include <stdio.h>

struct test {
	int one;
	int two;
	char *str;
	float flt;
};

int main(void) {
	printf("Struct's size is %d.\n", sizeof(struct test));
	return 0;
}
