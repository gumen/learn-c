#include <stdio.h>
#include <string.h>

int main(void) {
	char *myname = "David";
	char stringy[30];
	int i;
	char c;

	printf("%s, typa a string.\n", myname);
	fgets(stringy, 30, stdin);
	printf("\n");

	for(i = 0; i < strlen(stringy); i++) printf("%c", stringy[i]);
	for(i = strlen(stringy); i >= 0; i--) printf("%c", stringy[i]);
	printf("\n");

	return 0;
}
