#!/bin/sh

for source in *.c
do
	output=$(echo "$source" | sed "s/\.c$//")
	gcc -o "$output" "$source"
done
