#include <stdio.h>

struct users {
	int age;
	char gender;
	char *name;
	char *address;
};

void show_age(struct users u);

int main(void) {
	struct users user1, user2, user3;

	user1.age = 1;
	user1.gender = 'm';
	user1.name = "One";
	user1.address = "Mercury";

	printf("%d\n", user1.age);
	printf("%c\n", user1.gender);
	printf("%s\n", user1.name);
	printf("%s\n", user1.address);

	user2.age = 2;
	user2.gender = 'w';
	user2.name = "Two";
	user2.address = "Venus";

	show_age(user2);

	return 0;
}

void show_age(struct users user) {
	printf("%s %d\n", (&user)->name, user.age);
}
