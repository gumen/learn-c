#include <stdio.h>

int main(void) {
	int i = 5;

	do
		printf("%d\n", i);
	while(i--);

	return 0;
}
