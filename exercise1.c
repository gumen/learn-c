#include <stdio.h>
#include <stdlib.h>

// https://en.wikibooks.org/wiki/C_Programming/stdlib.h

int main(void)
{
	int x;
	int *px = &x;

	printf("Enter number\n");
	scanf("%d", px);
	printf("\n");

	while(*px > 0)
	{
		printf("%d\n", x--);
	}

	printf("Press ENTER to exit\n");
	/* I'm not sure why it needs double getchar.  Somehow it takes
	   ENTER key from last scanf. */
	getchar();
	getchar();

	return EXIT_SUCCESS;
}
