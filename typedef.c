#include <stdio.h>

typedef int Length;	// Length becomes synonym for int
typedef char * String;	// Looks like custom types are capitalized

// Define new type called "Pos" from struct
typedef struct position {
	int x;
	int y;
} Pos;

int main(void) {
	Pos button;
	button.x = 10;
	button.y = 20;

	printf("%d,%d\n", button.x, button.y);
	return 0;
}
