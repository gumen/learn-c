#include <stdio.h>

int main (void) {
	int x,y,z;
	x=1;
	y=2;

	int *ptoi;	// ptoi is, and stands for, pointer to integer
	ptoi=&x;	// ptoi points to x
	z=*ptoi;	// z is now 1, x's value
	ptoi=&y;	// ptoi now points to y

	printf("%d %d %d\n", x, y, z);

	return 0;
}
