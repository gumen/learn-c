#include <stdio.h>

int main(void) {
	char *string_a = "Test A";
	char string_b[] = "Test B";

	printf("%s\n", string_a);
	printf("%s\n", string_b);

	return 0;
}
