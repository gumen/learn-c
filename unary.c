#include <stdio.h>

// Unary operators to increment and decrement value

int main ()
{
	int x;
	int n = 10;
	int z;

	n++; // n will be 11 now
	++n;

	x = n++; // x will be 10
	z = ++n; // z will be 11

	return 0;
}
