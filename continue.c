#include <stdio.h>

int main(void) {
	for(int i = 5; i > 0; i--) {
		switch(i) {
		case 2: case 3: continue;
		}

		printf("%d\n", i);
	}

	return 0;
}
