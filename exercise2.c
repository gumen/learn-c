#include <stdio.h>
#include <stdarg.h>
/* https://en.wikipedia.org/wiki/Variadic_function#In_C */

double add(int count, ...) {
	va_list ap;
	double sum = 0;

	va_start(ap, count);
	for(int i = 0; i < count; i++)
		sum += va_arg(ap, int);
	va_end(ap);

	return sum;
}

int main(void) {
	printf("%f\n", add(3, 1, 2, 3));
	return 0;
}
