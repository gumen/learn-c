#include <stdio.h>

int main(void)
{
	/*
	infinite for loop:
	for(;;) { do_something(); }
	*/

	for(int i = 0; i < 5; i++) {
		printf("%d\n", i);
	}

	return 0;
}
