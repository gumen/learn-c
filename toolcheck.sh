#!/bin/sh

gcc -v 2>/dev/null || echo "GCC is not installed!"
ld -v >/dev/null || echo "Please install binutils!"
