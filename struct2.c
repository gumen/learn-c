#include <stdio.h>

int main(void) {
	struct users {
		int age;
		char gender;
		char *name;
		char *address;
	} user;

	user.age = 25;

	printf("Age: %d\n", user.age);

	return 0;
}
