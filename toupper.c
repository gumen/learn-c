#include <stdio.h>
#include <ctype.h>

/* https://en.wikibooks.org/wiki/C_Programming/ctype.h */

int main(void) {
	int c;	// the character read
	while ((c = getchar()) != EOF)
		putchar(toupper(c));
	return 0;
}
