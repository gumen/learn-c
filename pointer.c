#include <stdio.h>

int var = 20;
int *refvar = &var;

int main()
{
	printf("Pod adresem %d jest liczba: %d\n", refvar, *refvar);
	printf("Pod adresem %d jest liczba: %d\n", &var, var);
	return 0;
}
